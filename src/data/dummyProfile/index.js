import {FotoProfile} from '../../assets';

export const dummyProfile = {
  nama: 'Valin Fernanda',
  email: 'valinfernanda08@gmail.com',
  nomerHp: '0899 6881 537',
  alamat: 'Jl. Karasak Utara',
  kota: 'Bandung',
  provinsi: 'Jawa Barat',
  avatar: FotoProfile,
};
