export const heightMobileUI = 896;
export const widthMobileUI = 414;

export const API_KEY = '5eb209dd6212ce0b3f07c7769894eb2b';
export const API_RAJAONGKIR = 'https://api.rajaongkir.com/starter/';
export const API_HEADER_RAJAONGKIR = {
  key: API_KEY,
};
export const API_TIMEOUT = 120000;
